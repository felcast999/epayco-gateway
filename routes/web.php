<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which

| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//no auth routes

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->post('login', [
        'uses' => 'AuthController@login'
   ]);
   
   
   $router->get('logout', [
       'middleware'=>'auth',
       'uses' => 'AuthController@logout'
   ]);
   
   
   $router->get('user', [
       'middleware'=>'auth',
       'uses' => 'AuthController@authUser'
   ]);
   
   $router->post('register', [
       'uses' => 'GateWayController@send'
   ]);
    
});


//auth routes

$router->group(['middleware' => 'auth'], function () use ($router) {

$router->addRoute(['GET','POST', 'PUT', 'PATCH', 'DELETE','OPTIONS'], '/{params:.*}', function(Request $request){

   $controller= new \App\Http\Controllers\GateWayController;

   return $controller->send($request);


});




});
