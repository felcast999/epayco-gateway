<?php

declare(strict_types = 1);

return [
    'laravel' => [
        'base_uri' => env('LARAVEL_BASE_URI',"http://localhost:8080"),
        'secret' => env('LARAVEL_BASE_SECRET')
    ]
];
