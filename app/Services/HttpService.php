<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class HttpService
{
   protected $type;
   protected $url;
   protected $data;
   protected $files;
   protected $http;
   
   protected $auth_user_id;

   public function __construct(
      $type, 
      $url, 
      $data = [], 
      $token = null, 
      $files = []
   )
   {
      $this->type = $type;
      $this->url = $url;
      $this->data = $data;
      $this->token = $token;
      $this->files = $files;

      $this->auth_user_id = request()->header('Auth-User-Id');

      $default_headers = [
         'X-Requested-With' => 'XMLHttpRequest'
      ];

      if( $this->auth_user_id )
         $default_headers['Auth-User-Id'] = $this->auth_user_id;

      $this->http = Http::withHeaders($default_headers);

      if( $token )
         $this->http = $this->http->withToken($token);
   }

   public function send()
   {      
      switch ($this->type) {

         case 'GET':

            $encoded_url = http_build_query($this->data);
            return $this->http->get("{$this->url}?{$encoded_url}");
            break;

         case 'POST':
            return $this->post();
            break;

         case 'PUT':
            return request()->header('backend-provider') === 'gis' ? $this->put() : $this->post();
            break;

         case 'PATCH':
            return $this->http->patch($this->url, $this->data);
            break;

         case 'DELETE':
            return $this->http->delete($this->url);
            break;

         case 'OPTIONS':
            return $this->http->options($this->url);
            break;
      }
   }

   private function post()
   {
      $this->formatDataIfRequired();

      return count($this->files) ?
      $this->http->post($this->url) :
      $this->http->post($this->url, $this->data);
   }

   private function formatDataIfRequired()
   {        
      if( $this->files )
      {
         $file_keys = array_keys($this->files);
         
         foreach ($this->files as $key => $value)
         {
            if( is_array( $value ) )
            {
               foreach ($value as $index => $file)
               {
                  $_key = "{$key}[{$index}]";
                  $this->http = $this->http->attach($_key, file_get_contents($file), $file->getClientOriginalName());
               }
            }
            else
            {
               $this->http = $this->http->attach($key, file_get_contents($value), $value->getClientOriginalName());
            }
         }

         foreach ($this->data as $key => &$value) 
         {
            if ( ! in_array($key, $file_keys) )
            {
               if( is_array($value) )
               {
                  $this->formatArray($key, $value);
               }
               else
               {
                  $this->http = $this->http->attach($key, $value);
               }
            }
         }
      }
   }

   private function put()
   {
      unset( $this->data['_method']);

      $this->formatDataIfRequired();

      return count($this->files) ?
      $this->http->put($this->url) :
      $this->http->put($this->url, $this->data);
   }

   private function formatArray(string $key, array $data)
   {
      foreach ($data as $index => $value)
      {
         if( is_array($value) )
         {
            foreach ($value as $value_key => $value_data)
            {
               $_key = "{$key}[{$index}][{$value_key}]";
               $this->http = $this->http->attach($_key, $value_data);
            }
         }
         else
         {
            $_key = "{$key}[{$index}]";
            $this->http = $this->http->attach($_key, $value);
         }
      }
   }
}
