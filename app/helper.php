<?php

use Illuminate\Http\Request;

if (!function_exists('formatResponse'))
{
  function formatResponse($response)
  {
    $body = $response->getBody()->getContents();
      
    $status_code = $response->getStatusCode();

    $status_code_to_string = strval($response->getStatusCode());

    $_response = null;

    switch( $status_code_to_string[0] )
    {
      case "1":
      case "2":
      case "3":

          if( $response->header("Content-Type") === 'application/json' )
          {
              $body = json_decode($body);   
              $_response = response()->json($body, $status_code);
          }
          else
          {
              $_response = response($body, $status_code);
          }

        break;
      
      case "4":

          if( $response->header("Content-Type") === 'application/json' )
          {
              $errors = array_values(json_decode($body, true));
      
              $_response = response()->json(["errors" => $errors], $status_code);
          }
          else
          {
            $_response = response($body, $status_code);
          }

        break;

      case "5":

        $_response = response($body, $status_code);

        break;
    }

    return $_response;
  }
}

if (!function_exists('getFormResponse'))
{
  function getFormResponse($response)
  {

    $reason = $response->getReasonPhrase();
    $body = $response->getBody()->getContents();
    $status = $response->getStatusCode();

    switch ($status)
    {
      case '422':
        
        $errors = array_values(json_decode($body, true));

        return response()->json(["errors" => $errors], 422);

        break;

      case '500':

        return response($body, $status);

        break;
    }

    return response()->json(json_decode($body), $status);
  }
}

if (!function_exists('getBackendUrl')) {

  function getBackendUrl(Request $request)
  {

    $header = $request->header('backend-provider');
    $url = config('services.laravel.base_uri');
    return $url;
  }
}
