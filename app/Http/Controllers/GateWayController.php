<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\HttpService;

class GateWayController extends Controller
{
    public function send(Request $request)
    {

        $formatted_url = getBackendUrl($request) . $request->getRequestUri();

        $http = new HttpService(
          $request->method(),
          $formatted_url,
          $request->all(),
          $request->bearerToken(),
          $request->allFiles()
        );
        
        $response = $http->send();
        
        return formatResponse($response);
    }
}
