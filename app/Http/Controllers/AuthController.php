<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\HttpService;
use Illuminate\Support\Facades\Auth;
use Mtownsend\XmlToArray\XmlToArray;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        $formatted_url = getBackendUrl($request) . $request->getRequestUri();

        $http = new HttpService(
          $request->method(),
          $formatted_url,
          $request->all(),
          $request->bearerToken(),
          $request->allFiles()
        );
        
        $formatted_response = formatResponse($http->send());
        
        if( $formatted_response->getStatusCode() === 200 )
        {

            $user_in_json = json_decode($formatted_response->getContent(),true);
            $user_in_json =$user_in_json["data"];

            $user = User::find($user_in_json["id"]);
            
            $user->tokens->each(function($token) { $token->delete(); });

            unset($user->tokens);

            $token = $user->createToken('user token')->accessToken;
            
            return response()->json([
                'user' => $user_in_json,
                "authorization" => [
                    'token' => $token,
                    'type' => "Bearer"
                ]
            ]);
        }
        else
        {
            return $formatted_response;
        }
     
    }

    public function logout()
    {
        Auth::guard('api')->user()->tokens->each(function($token) { $token->delete(); });

        return response()->json([
            'message' => 'Sesion cerrada exitosamente'
        ]);
    }

    public function authUser(Request $request)
    {
        $url = str_replace('/smart-gest-gateway-backend/public', '', $request->getRequestUri());

        $formatted_url = getBackendUrl($request) . $url;

        $http = new HttpService('GET', $formatted_url, $request->all());

        $response = $http->send();

        return formatResponse($response);
    }
}