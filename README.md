<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Epayco Gateway

## Pasos para instalar:



- Clonar respositorio
- Ejecutar composer install
- Configurar archivo .env
- Crear variables de entorno `php artisan key:generate`
- Añadir `LARAVEL_BASE_URI="http://localhost:8080"` Por defecto, el backend usara el puerto 8080 y el gateway el puerto 8000
- Instalar passport `php artisan passport:install`
- Levantar el servidor `php -S localhost:8000 -t public`





